
function setup() {
    createCanvas(windowWidth, windowHeight);
}

function draw() {

    background(255);

    let time = floor(millis()/1000); // Returns the number of milliseconds (thousandths of a second) since starting the sketch
    
    noStroke();
    fill(58, 78, 255); // the fourth parameter is the alpha value.
    
    let x = 100; 
    let numberOfObjects = 12;

    for (let i = 0; i < time && i < numberOfObjects; i++) {  // the for-loop executes the code inside until a given condition is met (initial state; condition; iteration)

        rect(x, 100, 20, 40);

        x += 25; // ændre firkantens x-værdi for hver gang for-loop'et køres igennem
    }

    print("time: " + time + ", x-coordinate: " + x)

}





