
let sentenceArray = ["sims is nice", "i love this game", "wanna play?", "this is fun"];
let number = 0;


function preload() { //Preload images, JSON-files and font
  font = loadFont('excluded_font.ttf');
  backdrop = loadImage('blue.jpeg');
}

function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);
  textFont(font);
  textSize(16);
  rectMode(CENTER);
  textAlign(CENTER);
}

function draw() {


 
  for (let i = 0; i < number && i < sentenceArray.length; i++) {

    push();
    translate(-windowWidth/2, -windowHeight/2)
    image(backdrop, 0,0, windowWidth, windowHeight);
    pop();
    fill(0);
    text(sentenceArray[i], 0, 100, 400, 20);
  }

  if (number > sentenceArray.length) {
    number = 0;
  } else {
    number = number + 1 * (deltaTime / 1000);
  }

 
  push();
  ambientLight(20);
  directionalLight(50, 255, 29, -1, 1, -0.5);
  rotateY(millis()/1000);
  noStroke();
  fill(50, 255, 29);
  cone(60, 100, 7, 1);
  translate(0, -100, 0);
  cone(-60, -100, 7, 1);
  pop();
}
