

// Global variables
let circleX;
let speed = 2;
let r = 50;
let b = 205;
let g = 50;

function setup() {
    createCanvas(windowWidth, windowHeight);
    circleX = windowWidth / 2 - 360 // the circle's starting x-position
}

function draw() {

    // Green Square
    fill(178, 236, 93)
    noStroke();
    rectMode(CENTER);
    rect(windowWidth / 2, windowHeight / 2, 800, 500);

    // Dark green circle
    fill(r, b, g)
    circle(circleX, windowHeight / 2, 80);
    circleX = circleX + speed; // changes the circle's x-position to make it move


    if (circleX > windowWidth / 2 - 350 && circleX < windowWidth / 2 + 350) { // make the eyes disappear close to the wall 

        // White eyes
        fill(255);
        circle(circleX - 15, windowHeight / 2, 20);
        circle(circleX + 15, windowHeight / 2, 20);

        // Black iris
        fill(0);
        circle(circleX - 15, windowHeight / 2 + 3, 10);
        circle(circleX + 15, windowHeight / 2 + 3, 10);
    }


    // conditional statemate that makes the circle move both BACK and FORTH
    if (circleX <= windowWidth / 2 - 360) {
        speed = speed * -1;
        r = 50;
        b = 205;
        g = 50;

    } else if (circleX >= windowWidth / 2 + 360) {
        speed = speed * -1;
        r = 232;
        b = 244;
        g = 140;
    }
}

